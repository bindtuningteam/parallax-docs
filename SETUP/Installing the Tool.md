**PER SITE COLLECTION**

These steps will activate or deactivate the Tool for a single site collection where it's already installed. Repeat steps 3 onward for each site collection where the Tool is to be installed.

1. Download the latest version of the Tool from your BindTuning account;

2.	Open **Site Settings** at the root of your site collection;

	![Picture1.png](https://bitbucket.org/repo/yryydk/images/410606099-Picture1.png)

3.	Select Solutions under **Web Designer Galleries**;
	
	![Picture2.png](https://bitbucket.org/repo/yryydk/images/1669403944-Picture2.png)

4.	Click **Upload Solution**, on the top ribbon;

5. Upload the .wsp file from the Tool’s package;
6.	Click **OK** and wait for the dialog to finish loading;
7.	Click **Activate**.

The Tool is now installed and activated for this site collection. ✅ 