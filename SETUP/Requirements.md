### Requirements

- SharePoint On-Premises or SharePoint Online
- Site Collection Administration rights

###Browsers
Bindtuning themes should work in all modern browsers:

- Firefox
- Chrome
- Safari
- Opera
- IE9+
