These steps will help you use the tool properly. Here we go:

1.	Click on the gear icon to open the settings menu; 2.	Click **Edit Page**;3.	Click **Edit Source**;

	![Picture5.png](https://bitbucket.org/repo/yryydk/images/1252490138-Picture5.png)
	
4. Paste the following HTML and replace the placeholders with your own values:       
	           
	<pre>
		&lt;div id="ELEMENTID"&gt;&lt;img data-bt-parallax="ELEMENTID" data-bt-parallax-speed="0.35" src=" http://bit.ly/2fyTz8g" data-height="500" width="200"&gt;&lt;/div&gt;
	</pre>

5. Click **Ok** and then **Save**. 

<table>
  <tr>
    <th>Attribute</th>
    <th>Value</th>
  </tr>
  <tr>
    <td>data-bt-parallax</td>
    <td>Element ID (required)</td>
  </tr>
  <tr>
    <td>data-bt-parallax-speed</td>
    <td>Value from 0 to 1 (if unset takes in 0.65)</td>
  </tr>
  <tr>
    <td>data-height</td>
    <td>Value in PX (if unset takes in 100%)</td>
  </tr>
  <tr>
    <td>data-width</td>
    <td>Value in PX (if unset takes in 100%)</td>
  </tr>
</table>
