# BindTuning Parallax

This documentation was written by, and is property of Bind Lda (aka BindTuning), Portugal. As with any software product that constantly evolves, our products are in constant evolution. If you can’t find an answer to your questions by reading this manual, please contact BindTuning directly.

##### Document Revision Date
10.2.2016

##### Document Revision Number
1.0
