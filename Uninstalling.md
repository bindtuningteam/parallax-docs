**PER SITE COLLECTION**

## 1# Deactivate the Tool

These steps will deactivate Parallax for a single site collection where it's already installed.

1. Open **Site Settings** at the root of your site collection;

2.	Under **Site Collection Administration**, select **Site Collection Features**;

	<p class="alert-success">If you are are not seeing the **Site Collection Features**, click on **Go to top level site settings**  first and then **Site Collection Features**.</p> 
	
	![Picture3.png](https://bitbucket.org/repo/yryydk/images/3661954256-Picture3.png)

3.	Look for the **BindTuning Parallax**;
4.	Click **Deactivate**, to deactivate the tool.

The Tool is now deactivated for this site collection. ✅ 


## 2# Uninstall the Tool

These steps will fully remove the Tool from your site collection.

1.	Open **Site Settings** at the root of your site collection;

2.	Select **Solutions** under **Web Designer Galleries**;

3.	Select **BTParallax**;

4.	Open the item dropdown and choose the **Deactivate** option;

	![Picture4.png](https://bitbucket.org/repo/yryydk/images/30747356-Picture4.png)

5.	Press the **Deactivate** button on the dialog and refresh the page;

6.	Open the item dropdown and press the **Delete** option.

The tool is now fully removed from your site collection. ✅
